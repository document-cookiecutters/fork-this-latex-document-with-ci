for document in *.tex; do
  [ -f "$document" ] || break
  printf 'pdflatex %s\n' "$document"
  pdflatex $document
done

